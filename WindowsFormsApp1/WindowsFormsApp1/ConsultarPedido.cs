﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DB.Pedido;

namespace WindowsFormsApp1
{
    public partial class Consultar_Pedido : UserControl
    {
        public Consultar_Pedido()
        {
            InitializeComponent();
        }

        private void Consultar_Pedido_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoBusiness business = new PedidoBusiness();
                List<PedidoConsultar> lista = business.Consultar(txtCliente.Text.Trim());

                dgvPedidos.AutoGenerateColumns = false;
                dgvPedidos.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5)
                {
                    PedidoConsultar pedido = dgvPedidos.Rows[e.RowIndex].DataBoundItem as PedidoConsultar;

                    DialogResult r = MessageBox.Show($"Deseja realmente excluir o pedido {pedido.Id}?", "ELPW",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        PedidoBusiness business = new PedidoBusiness();
                        business.Remover(pedido.Id);

                        button1_Click(null, null);
                    }
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
