﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DB.Pedido;
using WindowsFormsApp1.DB.Cliente;
using WindowsFormsApp1.DB.Produto;

namespace WindowsFormsApp1
{
    public partial class Pedido : UserControl
    {

        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        BindingList<ClienteDTO> clientes = new BindingList<ClienteDTO>();

        public Pedido()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }


        void CarregarCombos()
        {
            comboBox2.SelectedIndex = 0;


            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            comboBox1.ValueMember = nameof(ProdutoDTO.Id);
            comboBox1.DisplayMember = nameof(ProdutoDTO.Nome);
            comboBox1.DataSource = lista;



            ClienteBusiness clienteBusiness = new ClienteBusiness();
            List<ClienteDTO> clienteLista = clienteBusiness.Listar();
            clientes = new BindingList<ClienteDTO>(clienteLista);

            comboBox3.ValueMember = nameof(ClienteDTO.Id);
            comboBox3.DisplayMember = nameof(ClienteDTO.Nome);
            comboBox3.DataSource = clientes;
        }

        void ConfigurarGrid()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = produtosCarrinho;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cliente = comboBox3.SelectedItem as ClienteDTO;

                PedidoDTO dto = new PedidoDTO();
                dto.FuncionarioId = UserSession.UsuarioLogado.Id;
                dto.ClienteId = cliente.Id;
                dto.FormaPagamento = comboBox2.Text;
                dto.Data = DateTime.Now;

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto, produtosCarrinho.ToList());

                MessageBox.Show("Pedido salvo com sucesso.", "ELPW", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

