﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DB.Pedido
{
    class PedidoBusinessItem
    {
        public int Salvar(PedidoDTOItem dto)
        {
            PedidoDatabaseItem db = new PedidoDatabaseItem();
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            PedidoDatabaseItem db = new PedidoDatabaseItem();
            db.Remover(id);
        }

        public List<PedidoDTOItem> ConsultarPorPedido(int idPedido)
        {
            PedidoDatabaseItem db = new PedidoDatabaseItem();
            return db.ConsultarPorPedido(idPedido);
        }

    }
}
