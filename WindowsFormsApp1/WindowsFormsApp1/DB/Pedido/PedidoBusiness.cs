﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.DB.Produto;

namespace WindowsFormsApp1.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            if (pedido.FormaPagamento == "Selecione")
            {
                throw new ArgumentException("Forma de Pagamento é obrigatório.");
            }

            if (pedido.ClienteId == 0)
            {
                throw new ArgumentException("Cliente é obrigatório.");
            }

            if (pedido.FuncionarioId == 0)
            {
                throw new ArgumentException("Funcionário é obrigatório.");
            }

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoBusinessItem itemBusiness = new PedidoBusinessItem();
            foreach (ProdutoDTO item in produtos)
            {
                PedidoDTOItem itemDto = new PedidoDTOItem();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }

        public void Remover(int pedidoId)
        {
            PedidoBusinessItem itemBusiness = new PedidoBusinessItem();
            List<PedidoDTOItem> itens = itemBusiness.ConsultarPorPedido(pedidoId);

            foreach (PedidoDTOItem item in itens)
            {
                itemBusiness.Remover(item.Id);
            }

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            pedidoDatabase.Remover(pedidoId);
        }

        public List<PedidoConsultar> Consultar(string cliente)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            return pedidoDatabase.Consultar(cliente);
        }

    }
}
