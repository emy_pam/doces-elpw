﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp1.DB.Base;

namespace WindowsFormsApp1.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_produto (nm_produto, vl_preco, img_produto, id_categoria) 
                                   VALUES (@nm_produto, @vl_preco, @img_produto, @id_categoria)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            parms.Add(new MySqlParameter("img_produto", dto.Imagem));
            parms.Add(new MySqlParameter("id_categoria", dto.CategoriaId));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE tb_produto 
                                 SET nm_produto  = @nm_produto,
                                     vl_preco    = @vl_preco,
                                     img_produto = @img_produto
                               WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.Id));
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            parms.Add(new MySqlParameter("img_produto", dto.Preco));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoConsultar> Consultar(string produto)
        {
            string script = @"SELECT * FROM VW_PRODUTO_CONSULTA WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoConsultar> lista = new List<ProdutoConsultar>();
            while (reader.Read())
            {
                ProdutoConsultar dto = new ProdutoConsultar();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Imagem = reader.GetString("img_produto");
                dto.Categoria = reader.GetString("nm_categoria");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Imagem = reader.GetString("img_produto");
                dto.CategoriaId = reader.GetInt32("id_categoria");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
