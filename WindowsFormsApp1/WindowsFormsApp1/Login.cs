﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DB.Funcionario;

namespace WindowsFormsApp1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
                try
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    FuncionarioDTO funcionario = business.Logar(textBox2.Text, textBox1.Text);

                    if (funcionario != null)
                    {
                        UserSession.UsuarioLogado = funcionario;

                        Form1 menu = new Form1();
                        menu.Show();
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Credenciais inválidas.", "Doceria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message, "ELPW",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocorreru um erro, tente mais tarde.", "ELPW",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            
        }
    }
}
