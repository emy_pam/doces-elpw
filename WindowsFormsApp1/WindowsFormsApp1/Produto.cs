﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DB.Produto;

namespace WindowsFormsApp1
{
    public partial class Produto : UserControl
    {
        public Produto()
        {
            InitializeComponent();
        }

        private void Produto_Load(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
             {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Nome = txtProduto.Text.Trim();
                dto.Preco = Convert.ToDecimal(txtPreco.Text.Trim());

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto salvo com sucesso.", "ELPW",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
