﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DB.Cliente;

namespace WindowsFormsApp1
{
    public partial class Cliente : UserControl
    {
        public Cliente()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cliente = new ClienteDTO();
                cliente.Nome = textBox2.Text;
                cliente.Cpf = textBox1.Text;
                cliente.Telefone = Convert.ToInt32(textBox3.Text);
                cliente.Endereco = textBox4.Text;

                ClienteBusiness business = new ClienteBusiness();
                business.Salvar(cliente);

                MessageBox.Show("Cliente Salvo com sucesso.", "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);


               
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "ELPW",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
